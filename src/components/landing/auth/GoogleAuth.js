import React from "react"
import { GoogleLogin } from "react-google-login"
import WrapperGoogleAuth from "./WrapperGoogleAuth"

const responseGoogle = (response) => {
	return (
		<WrapperGoogleAuth>
			<div className='container'>
				<GoogleLogin
					clientId='600236243869-1hh8pgmgb28b0kusr8g8qv43qjnna40j.apps.googleusercontent.com'
					render={(renderProps) => (
						<button
							className='loginbutton'
							onClick={renderProps.onClick}
							disabled={renderProps.disabled}
						>
							<img
								className='logobutton'
								src='/assets/images/logo.png'
								alt=''
							/>
						</button>
					)}
					buttonText='Login'
					onSuccess={responseGoogle}
					onFailure={responseGoogle}
					cookiePolicy={"single_host_origin"}
				/>
			</div>
		</WrapperGoogleAuth>
	)
}
export default responseGoogle
