import styled from "styled-components"

const WrapperGoogleAuth = styled.div`
	.container {
		position: absolute !important;
		top: 0;
		bottom: 0;
		right: 0;
		left: 0;
		display: flex;
		justify-content: center;
		align-items: center;
	}
	@media screen and (max-width: 400px) {
		.loginbutton {
			display: flex;
			position: absolute !important;
			background: #ffffff;
			border: 0.5px solid #000000;
			box-sizing: border-box;
			box-shadow: 0px 4px 4px rgb(0 0 0 / 25%), 0px 4px 4px rgb(0 0 0 / 25%);
			border-radius: 25px;
			top: 65%;
			margin-bottom: auto;
			width: 206px;
			height: -15%;
			-ms-flex-align: center;
			-ms-flex-pack: center;
			cursor: pointer;
		}
	}
	.loginbutton {
	}
	.logobutton {
		margin-left: auto;
		margin-right: auto;
	}
`
export default WrapperGoogleAuth
