import styled from "styled-components"

const WrapperLanding = styled.div`
	.container {
		position: absolute !important;
		top: 0;
		bottom: 0;
		right: 0;
		left: 0;
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.Typewriter {
		margin-left: 10px;
		margin-right: auto;
		justify-content: center;
		text-align: center;
		display: contents;
		justify-content: center;
		font-size: 30px;
		font-weight: 100;
		font-style: normal;
		font-weight: bold;
		font-size: 36px;
		line-height: 42px;
	}
	.Typewriter__wrapper span {
		color: white;
		background-color: #231e46;
		text-align: justify;
	}

	.Typewriter__cursor {
		visibility: hidden;
	}
	.frame {
		position: absolute !important;
		top: 10%;
		max-width: 33%;

		align-items: center;
		justify-content: center;
		vertical-align: top;
	}
	@media screen and (max-width: 400px) {
		.iconspan {
			position: absolute !important;
			bottom: 0;
			right: 0;
			left: 0;
			display: flex;
			justify-content: center;
			align-items: center;
		}
	}
	@media screen and (max-width: 400px) {
		.icon {
			position: absolute !important;
			bottom: 0;
			right: 0;
			left: 30%;
			display: flex;
			justify-content: center;
			align-items: center;
		}
	}

	.iconspan {
		position: absolute !important;
		bottom: 20px;
		right: 0;
		margin-right: 10px;
		font-weight: 700;
	}
	.icon {
		position: absolute !important;
		bottom: 17px;
		right: 0;
		margin-right: 112px;
	}

	.loginbutton {
		display: flex;
		position: absolute;
		background: #ffffff;
		border: 0.5px solid #000000;
		box-sizing: border-box;
		box-shadow: 0px 4px 4px rgb(0 0 0 / 25%), 0px 4px 4px rgb(0 0 0 / 25%);
		border-radius: 25px;
		top: 59%;
		margin-bottom: auto;
		width: 206px;
		height: -15%;
		-ms-flex-align: center;
		-ms-flex-pack: center;
		cursor: pointer;
	}
	.logobutton {
		margin-left: auto;
		margin-right: auto;
	}
`
export default WrapperLanding
