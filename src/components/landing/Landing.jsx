import React from "react"
import Header from "../header/Header"
import Typewriter from "typewriter-effect"
import WrapperLanding from "./WrapperLanding"
import ResponseGoogle from "./auth/GoogleAuth"
const Landing = () => {
	return (
		<WrapperLanding>
			<Header />
			<div className='container'>
				<img className='frame' src='/assets/images/Auth.svg' alt='' />
				<Typewriter
					className='typewriter'
					onInit={(typewriter) => {
						typewriter
							.typeString(
								'<span className="span" >Add</span> <span className="span" >Modify</span> <span className="span" >Control</span> ',
							)
							.typeString(" and Trace your devices !")
							.start()
					}}
				/>
				<ResponseGoogle />

				<img className='icon' src='/assets/images/iconinternet.svg' alt='' />
				<span className='iconspan'>www.oyez.fr</span>
			</div>
		</WrapperLanding>
	)
}

export default Landing
