import React from "react"
import WrapperHeader from "./WrapperHeader"

const Header = () => {
	return (
		<WrapperHeader>
			<div>
				<img className='logo' src='/assets/images/Logo.svg' alt='' />
				<span className='vertical'></span>
				<span className='horizontal'></span>
			</div>
		</WrapperHeader>
	)
}

export default Header
