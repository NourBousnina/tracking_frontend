import styled from "styled-components"

const WrapperHeader = styled.div`
	.logo {
		position: absolute !important;
		top: 3%;
		bottom: 0;
		right: 0;
		left: 2%;

		display: flex;
		max-width: 10%;
		min-width: 8%;
	}
	.vertical {
		border-right: 4px solid #fdd67f;
		transform: matrix(-1, 0, 0, 1, 0, 0);
		height: 94%;
		position: absolute;
		left: 2%;
		bottom: 0;
		top: 5%;
		right: 0;
	}
	.horizontal {
		border-bottom: 4px solid #fdd67f;
		transform: matrix(-1, 0, 0, 1, 0, 0);
		height: 95%;
		position: absolute;
		left: 2%;
		bottom: 0;
		top: 4%;
		right: 0;
	}
`

export default WrapperHeader
